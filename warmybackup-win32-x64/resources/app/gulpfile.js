'use strict';

let gulp = require('gulp');
let webpack = require('webpack');
let strm_webpack = require('webpack-stream');
let concat = require('gulp-concat');
let electron = require('electron-connect').server.create();
let plumber = require('gulp-plumber');

gulp.task('css', () => {
    return gulp.src(['./src/css/*.css'])
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('dist'));
});

gulp.task('webpack', (cb) => {
    return gulp.src('./src/components/main.tsx')
        .pipe(plumber())
        .pipe(
        strm_webpack({
            output:
            {
                filename: 'bundle.js'
            },
            target: 'electron',
            watch: true,
            resolve:
            {
                extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
            },
            module:
            {
                rules:
                [
                    {
                        test: /\.tsx?$/,
                        exclude: /(node_modules|bower_components)/,
                        loader: 'ts-loader'
                    }
                ]
            },
            plugins:
            [
                new webpack.DefinePlugin({
                    'process.env': {
                        // This has effect on the react lib size
                        'NODE_ENV': JSON.stringify('production'),
                    }
                }),
                //new webpack.optimize.DedupePlugin(),
                // new webpack.optimize.UglifyJsPlugin({
                //     compress: {
                //         warnings: false
                //     },
                //     mangle: {
                //         except: ['$super', '$', 'exports', 'require']
                //     },
                //     sourceMap: false
                // })
            ]
        }, webpack))
        .pipe(gulp.dest('./dist'));
});

let electronStart = (cb) => {
    electron.start();
    cb();
};

let electronRestart = (cb) => {
    electron.restart();
    cb();
};

let electronReload = (cb) => {
    electron.reload();
    cb();
};

gulp.task('watch-main', () => {
    return gulp.watch('main.js', gulp.series(electronRestart));
});

gulp.task('watch-render', () => {
    return gulp.watch(['./dist/styles.css', './index.html', './dist/bundle.js'], gulp.series(electronReload));
});

gulp.task('watch-css', () => {
    return gulp.watch(['./src/css/*.css'], gulp.series('css'));
});

gulp.task('default', gulp.series('css', electronStart, gulp.parallel('webpack', 'watch-css', 'watch-main', 'watch-render')));