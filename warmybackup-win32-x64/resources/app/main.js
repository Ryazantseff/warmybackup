const Electron = require('electron');
const path = require('path');
const url = require('url');
//const client = require('electron-connect').client;

let MainWindow;

Electron.app.on('ready', () => {
    MainWindow = new Electron.BrowserWindow({ width: 650, height: 900 });
    MainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));
    MainWindow.on('closed', () => {
        MainWindow = null;
    });
  //  client.create(MainWindow);
});

Electron.app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        Electron.app.quit();
    }
});

Electron.app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});
