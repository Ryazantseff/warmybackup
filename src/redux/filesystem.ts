//import * as Redux from 'redux';
import * as Immutable from "immutable";
import * as FS from "fs";
import * as Path from "path";



export const Files = (state: any, action: any) => {
    if (!state) state = Immutable.Map({
        files: Immutable.List([]),
        message: ""
    });
    switch (action.type) {
        case "READ_DIR_COMMIT":
            return state.update("files", v => Immutable.fromJS(action.items));
        case "INDICATION":
            return state.update("message", v => action.message);
    }
    return state;
}


export const FilesMiddleware = store => next => async action => {
    switch (action.type) {
        case "READ_DIR_INIT":
            let path = Path.resolve(action.path);
            FS.readdir(path, (err, items) => {
                if (items) {
                    store.dispatch({
                        type: "READ_DIR_COMMIT",
                        items: items.map(v => {
                            return ({
                                name: v,
                                path: path,
                                isdir: FS.statSync(Path.resolve(path + "/" + v)).isDirectory()
                            });
                        })
                    });
                }
            });
            break;
        case "RECURSIVE_READ_DIR":
            let statPromise = (file) => {
                return new Promise((resolve, reject) => {
                    FS.stat(file, (err, stat) => {
                        err ? reject(err) : resolve(stat.isDirectory());
                    });
                });
            }

            let readDirPromise = (path, indication) => {
                return new Promise((resolve, reject) => {
                    FS.readdir(Path.resolve(path), async (err, items) => {
                        if (!err) {
                            let result = [];
                            indication(Path.resolve(path + "/"));
                            for (let i in items) {
                                try {
                                    let tmp = await statPromise(Path.resolve(path + "/" + items[i]));
                                    result.push(tmp ?
                                        {
                                            name: items[i],
                                            path: path,
                                            isdir: tmp,
                                            content: await readDirPromise(Path.resolve(path + "/"
                                                                            + items[i]), indication)
                                        } : {
                                            name: items[i],
                                            path: path,
                                            isdir: tmp
                                        });
                                } catch (e) {
                                    console.log(e);
                                }

                            }
                            resolve(result);
                        }
                        else reject(err);
                    });
                });
            }

            store.dispatch({
                type: "READ_DIR_COMMIT",
                items: await readDirPromise(action.path, (message) => {
                    store.dispatch({
                        type: "INDICATION",
                        message: message
                    });
                })
            });

            break;

        default: return next(action);
    }

}