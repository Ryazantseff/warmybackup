import * as Redux from 'redux';

import {Files, FilesMiddleware} from "./filesystem";
import {Postgres, PostgresMiddleware} from "./postgress";


const Reducers = Redux.combineReducers(
    {
        files: Files,
        db: Postgres
    });


const Middleware = Redux.applyMiddleware(FilesMiddleware, PostgresMiddleware);

export const Store = Redux.createStore(Reducers, Middleware);