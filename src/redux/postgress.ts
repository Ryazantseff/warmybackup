import * as Immutable from "immutable";

import * as pgPromise from "pg-promise";

const PgClient = pgPromise()('postgres://warmyuser:rssgx14Rc@194.87.239.221:5432/warmybackup');

export const Postgres = (state: any, action: any) => {
    if (!state) state = Immutable.Map({
        tapes: Immutable.fromJS([])
    });
    switch (action.type) {
        case "TAPES_LIST":
            return state.update("tapes", v => Immutable.fromJS(action.tapes));

    }
    return state;
}

export const PostgresMiddleware = store => next => async action => {
    switch (action.type) {
        case "GET_LTO_TAPES":
            PgClient.tx((t) => {
                let queries = [
                    t.query("select * from lto_tape")
                ];
                return t.batch(queries);
            })
                .then((data) => {
                    // data[0].map(a=>console.log(a));

                    store.dispatch({
                        type: "TAPES_LIST",
                        tapes: data[0]
                    });
                })
                .catch((error) => {
                    console.log(error);
                    //  console.log(error);
                });
            break;
        case "ADD_LTO_TAPE":
            PgClient.tx((t) => {
                let queries = [
                    t.query("insert into lto_tape(barcode, comment) values (${barcode}, ${comments})", action),
                    t.query("select * from lto_tape")
                ];
                return t.batch(queries);
            })
                .then((data) => {
                    // console.log(data[0]);
                    store.dispatch({
                        type: "TAPES_LIST",
                        tapes: data[1]
                    });
                })
                .catch((error) => {
                    store.dispatch({
                        type: "INDICATION",
                        message: error.toString()
                    });
                });
            break;
        case "DEL_LTO_TAPE":
            PgClient.tx((t) => {
                let queries = [
                    t.query("delete from lto_tape where id = ${id}", action),
                    t.query("select * from lto_tape")
                ];
                return t.batch(queries);
            })
                .then((data) => {
                    store.dispatch({
                        type: "TAPES_LIST",
                        tapes: data[1]
                    });
                })
                .catch((error) => {
                    store.dispatch({
                        type: "INDICATION",
                        message: error.toString()
                    });
                });
            break;
        default: return next(action);
    }

}