import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { Store } from "../redux/reduxstore";
import MainView from "./mainview";

class Main extends React.Component<any, any> {
    render() {
        return (
            <Provider store={Store}>
                <MainView/>
            </Provider>
        );
    }
}

ReactDOM.render( <Main/> , document.getElementById('container'));





