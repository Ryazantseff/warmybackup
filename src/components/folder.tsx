import * as React from "react";
import * as ReactDOM from "react-dom";
import { connect } from 'react-redux';

class Folder extends React.Component<any, any> {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div> 
                <h1>{this.props.name}</h1>
                {this.props.childfolders}
                 <ul>
                    {this.props.files ? this.props.files.map(v => <li> { v.get("isdir") ? 
                        <Folder name={v.get("name")} /> : v.get("name")} </li>) : ""}
                </ul>
            </div>
        );
    }
}

function mapStateToProps(store: any) {
    return {
        files: store.files.get("files"),
        message: store.files.get("message")
    };
};
function mapDispatchToProps(dispatch: any) {
    return {
        readDir: (path) => {
            dispatch({
                type: "RECURSIVE_READ_DIR",
                path: path
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Folder) as any;