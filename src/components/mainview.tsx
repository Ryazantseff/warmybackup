import * as React from "react";
import * as ReactDOM from "react-dom";
import { connect } from 'react-redux';
import Folder from "./folder";
const dialog = require('electron').remote.dialog;

class MainView extends React.Component<any, any> {
    componentDidMount() {
        this.props.getTapes();
    }
    selectDir() {
        dialog.showOpenDialog({
            properties: ['openDirectory']
        }, (result) => { this.props.readDir(result[0]) });
    }
    addTape() {
        this.props.addTape((this.refs.barcode as any).value, (this.refs.comments as any).value);
        (this.refs.barcode as any).value = "";
        (this.refs.comments as any).value = "";
    }
    delTape(id) {
        this.props.delTape(id);
    }
    render() {
       // this.props.tapes.map(a => console.log(a));
        let tapes = this.props.tapes.map(a =>
            <div className="tapeitem">
                <p>{a.barcode}</p>
                <p>{a.comment}</p>
                <div>
                    <div key="deltape" className="button" onClick={e => this.delTape(a.id)}>Delete Tape</div>
                    <div key="scantape" className="button">Scan Tape</div>
                    <div key="edittape" className="button">Edit Tape</div>
                </div>
            </div>
        );
        return (
            <div>
                <h3>Trace:</h3> <p>{this.props.message}</p>
                <div key="read" className="button" onClick={e => this.selectDir()}>Read Dir</div>
                <div className="newtape">
                    <div>
                        <label>Barcode</label>
                        <input ref="barcode" />
                    </div>
                    <div>
                        <label>Comments</label>
                        <textarea ref="comments"></textarea>
                    </div>
                    <div key="add" className="button" onClick={e => this.addTape()}>Add Tape</div>
                </div>
                {tapes}
            </div >
        );
    }
}

function mapStateToProps(store: any) {
    return {
        files: store.files.get("files"),
        message: store.files.get("message"),
        tapes: store.db.get("tapes")
    };
};
function mapDispatchToProps(dispatch: any) {
    return {
        getTapes: () => {
            dispatch({
                type: "GET_LTO_TAPES"
            });
        },
        addTape: (barcode, comments) => {
            dispatch({
                type: "ADD_LTO_TAPE",
                barcode: barcode,
                comments: comments
            });
        },
        delTape: (id) => {
            dispatch({
                type: "DEL_LTO_TAPE",
                id: id
            });
        },
        readDir: (path) => {
            dispatch({
                type: "RECURSIVE_READ_DIR",
                path: path
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainView) as any;